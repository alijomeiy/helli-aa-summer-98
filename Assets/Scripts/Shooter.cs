﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] private GameObject pinPishsakhte;
    [SerializeField] private Vector3 speed;
    [SerializeField] private Transform pin;
    [SerializeField] private Transform circle;

    [SerializeField] private AudioSource getCoin;
    private Transform nextPin;
    bool spacePressedFlag = false;
    bool isCollisionHappend = false;
    bool isPrefabeCreated = false;

    private int pinCounter = 0;

    GameObject manager;
    GameManager gameManager;

    void Start()
    {
        manager = GameObject.Find("GameManager");
        gameManager = manager.GetComponent<GameManager>();
    }

    void Update()
    {
        if (gameManager.State == GameManager.AppState.PLAY)
        {
            ManageInputs();
            ManagePinShooting(pin);
            ManageNewPinCreation();
        }

        if (gameManager.IsGameFinished())
            pinCounter = 0;

    }

    private void ManageInputs()
    {
        // if (Input.GetKeyDown(KeyCode.Space) == true)
        if (Input.GetKeyDown(KeyCode.Space))
        {
            spacePressedFlag = true;
        }
    }

    private void ManagePinShooting(Transform inShootPin)
    {
        if (spacePressedFlag == true)
        {
            if (inShootPin.position.y < -1.76f)
            {
                inShootPin.position += speed;
            }

            else if (inShootPin.parent != circle)
            {
                inShootPin.parent = circle;
                isCollisionHappend = true;
                spacePressedFlag = false;
                isPrefabeCreated = false;
                pinCounter ++;
                getCoin.Play();
            }

            inShootPin.GetChild(0).gameObject.SetActive(true);
        }
    }

    private void ManageNewPinCreation()
    {
        // if (CanPinCreate() == true)
        if (CanPinCreate())
        {
            GameObject newPin = Instantiate
                (pinPishsakhte, transform.position, Quaternion.identity);
            newPin.name = "Pin";
            isPrefabeCreated = true;
            isCollisionHappend = false;
            pin = newPin.transform;
        }
    }

    private bool CanPinCreate()
    {
        // if (isCollisionHappend == true && isPrefabeCreated == false)
        //     return true;
        // else
        //     return false;
        return isCollisionHappend && !isPrefabeCreated;
    }

    public int GetPinCounter()
    {
        return pinCounter;
    }
}

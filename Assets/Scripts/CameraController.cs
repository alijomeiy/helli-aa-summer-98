﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Camera camera;
    [SerializeField] GameManager manager;
    [SerializeField] Color defaultColor;

    void Start()
    {
        camera = GetComponent<Camera>();
    }

    private void Update()
    {
        if (manager.IsGameStateInPlay())
            camera.backgroundColor = defaultColor;
    }

    public void ChangeCameraBackgroundColor(Color newColor)
    {
        print("** ChangeCameraBackgroundColor **");
        StartCoroutine(ChangeBackgroundColor(newColor));
    }

    private IEnumerator ChangeBackgroundColor(Color newColor)
    {
        while (newColor != camera.backgroundColor)
        {
            yield return new WaitForEndOfFrame();
            camera.backgroundColor = Color.Lerp(camera.backgroundColor, newColor, .003f);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float speed;
    GameObject manager;
    GameManager gameManager;

    void Start()
    {
        manager = GameObject.Find("GameManager");
        gameManager = manager.GetComponent<GameManager>();
    }

    void Update()
    {
        if (gameManager.State == GameManager.AppState.PLAY)
        {
            transform.Rotate(0f, 0f, speed);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    List<int> levels = new List<int>();
    private int level;
    public AppState State;
    [SerializeField] private CameraController mainCamera;
    [SerializeField] Shooter shooter;
    [SerializeField] Button startButton, resetButton;
    [SerializeField] Rotator rotator;

    private void Start()
    {
        level = 1;
        levels.Add(5);
        levels.Add(7);
        levels.Add(10);
        levels.Add(14);
        levels.Add(20);
    }

    void Update()
    {
        ManageChangeBackgroundColor();
        CheckLevelFinish();
    }

    private void CheckLevelFinish()
    {
        if (GetRemindedPinsCount() == 0)
        {
            State = AppState.WIN;
            level += 1;
        }
    }

    public int GetRemindedPinsCount()
    {
        int levelPins = levels[level - 1];
        int shootedPin = shooter.GetPinCounter();
        return levelPins - shootedPin;
    }

    public void ResetGame()
    {
        State = AppState.MAIN_MENU;
        startButton.gameObject.SetActive(true);
        resetButton.gameObject.SetActive(false);
        CleanMainCircle();
        // TODO: رنگ پس زمینه دوربین را به حالت اولیه برگردان
    }

    private void CleanMainCircle()
    {
        int childCount = rotator.transform.childCount;
        for (int i = 0; i < childCount; i ++)
        {
            Destroy(rotator.transform.GetChild(i).gameObject);
        }
    }

    private void ManageChangeBackgroundColor()
    {
        if (State == AppState.GAMEOVER)
        {
            mainCamera.ChangeCameraBackgroundColor(Color.red);
        }

        if (State == AppState.WIN)
        {
            mainCamera.ChangeCameraBackgroundColor(Color.green);
        }
    }

    public void StartGame()
    {
        State = AppState.PLAY;
    }

    public int GetLevel()
    {
        return level;
    }

    public List<int> GetLevels()
    {
        return levels;
    }

    public bool IsCurrentStateEqualTo(AppState state)
    {
        //if (State == state)
        //    return true;
        //return false;
        return State == state;
    }

    public bool IsGameStateInPlay()
    {
        return IsCurrentStateEqualTo(AppState.PLAY);
    }

    public bool IsGameFinished()
    {
        //if (IsCurrentStateEqualTo(AppState.WIN) || IsCurrentStateEqualTo(AppState.GAMEOVER))
        //    return true;
        //return false;
        return IsCurrentStateEqualTo(AppState.WIN) || IsCurrentStateEqualTo(AppState.GAMEOVER);
    }

    public enum AppState
    {
        MAIN_MENU,
        PLAY,
        GAMEOVER,
        WIN
    }
}
